#include <iostream>

#include "core.h"

namespace nlohmann {
void adl_serializer<std::shared_ptr<const arrow::KeyValueMetadata>>::to_json(
    json &j, const std::shared_ptr<const arrow::KeyValueMetadata> &metadata) {
  for (auto i = 0; i < metadata->size(); i++)
    j[metadata->key(i)] = metadata->value(i);
}

void adl_serializer<std::shared_ptr<arrow::Field>>::to_json(
    json &j, const std::shared_ptr<arrow::Field> &field) {
  j["arrow_type"] = "arrow_field";
  j["name"] = field->name();
  j["type"] = field->type();
  if (field->metadata() != nullptr) {
    j["metadata"] = field->metadata();
  }
  j["nullable"] = field->nullable();
}

void adl_serializer<std::shared_ptr<arrow::ListType>>::to_json(
    json &j, const std::shared_ptr<arrow::ListType> &type) {
  j["arrow_type"] = "arrow_datatype";
  j["type"] = type->id();
  j["field"] = type->value_field();
}

void adl_serializer<std::shared_ptr<arrow::DataType>>::to_json(
    json &j, const std::shared_ptr<arrow::DataType> &type) {
  if (type->id() == arrow::Type::LIST) {
    auto list_type =
        std::static_pointer_cast<arrow::ListType, arrow::DataType>(type);
    j = list_type;
  } else {
    j["arrow_type"] = "arrow_datatype";
    j["type"] = type->id();
  }
}

void adl_serializer<std::shared_ptr<arrow::Schema>>::to_json(
    json &j, const std::shared_ptr<arrow::Schema> &type) {
  j["arrow_type"] = "arrow_schema";
  json fields;
  for (const auto &field : type->fields())
    fields.push_back(field);
  j["fields"] = fields;
  if (type->metadata() != nullptr)
    j["metadata"] = type->metadata();
  // TODO: store endianness
}
} // namespace nlohmann
