#include <memory>
#include <vector>

#include "core.h"
#include <arrow/api.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

TEST(FieldTypeTest, SimpleFieldTest) {
  auto field = arrow::field("int32", arrow::int32());
  json j = field;
  ASSERT_EQ(j.at("arrow_type"), "arrow_field");
  ASSERT_EQ(j.at("name"), "int32");
  ASSERT_EQ(j.at("type"), json(arrow::int32()));
}

TEST(FieldTypeTest, ComplexNullableFieldTest) {
  auto keys = std::vector<std::string>{"one", "two", "three"};
  auto values = std::vector<std::string>{"1", "2", "3"};
  auto metadata = std::make_shared<const arrow::KeyValueMetadata>(keys, values);
  auto field = arrow::field("int32", arrow::int32(), true, metadata);
  json j = field;
  ASSERT_EQ(j.at("arrow_type"), "arrow_field");
  ASSERT_EQ(j.at("name"), "int32");
  ASSERT_EQ(j.at("type"), json(arrow::int32()));
  ASSERT_TRUE(j.at("nullable"));
  ASSERT_EQ(j.at("metadata"), json(metadata));
}
