#include "core.h"
#include <arrow/api.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

#include <memory>
#include <vector>

using json = nlohmann::json;

TEST(MetadataTest, SimpleMetadataTest) {
  auto keys = std::vector<std::string>{"one", "two", "three"};
  auto values = std::vector<std::string>{"1", "2", "3"};
  auto metadata = std::make_shared<const arrow::KeyValueMetadata>(keys, values);
  json j = metadata;
  ASSERT_EQ(j.at("one"), "1");
  ASSERT_EQ(j.at("two"), "2");
  ASSERT_EQ(j.at("three"), "3");
}
