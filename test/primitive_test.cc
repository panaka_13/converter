#include "core.h"
#include <arrow/api.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

#define TEST_ARROW(type, factory)                                              \
  TEST(PrimitiveTest, type##Test) {                                            \
    auto type = arrow::factory();                                              \
    json j = type;                                                             \
    ASSERT_EQ(j.at("arrow_type"), "arrow_datatype");                           \
    ASSERT_EQ(j.at("type"), type->id());                                       \
  }

TEST_ARROW(FloatType, float32);
TEST_ARROW(DoubleType, float64);
TEST_ARROW(Int32Type, int32);
TEST_ARROW(Int64Type, int64);
