#include "core.h"
#include <arrow/api.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

#include <iostream>

TEST(ListTypeTest, SimpleListTest) {
  auto field = arrow::field("int32", arrow::int32());
  auto list = arrow::list(field);
  json j = list;
  ASSERT_EQ(j.at("arrow_type"), "arrow_datatype");
  ASSERT_EQ(j.at("type"), list->id());
  ASSERT_EQ(j.at("field"), json(field));
}

TEST(ListTypeTest, NestedListTest) {
  auto field = arrow::field("int32", arrow::int32());
  auto list = arrow::list(field);
  auto nested_list = arrow::list(list);
  json j = nested_list;
  ASSERT_EQ(j.at("arrow_type"), "arrow_datatype");
  ASSERT_EQ(j.at("type"), list->id());
  ASSERT_EQ(j.at("field"), json(arrow::field("item", list)));
}
