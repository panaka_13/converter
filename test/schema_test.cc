#include <memory>
#include <vector>

#include "core.h"
#include <arrow/api.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

TEST(SchemaTypeTest, SimpleSchemaTest) {
  auto int32_field = arrow::field("int32_field", arrow::int32());
  auto int64_field = arrow::field("int64_field", arrow::int64());
  auto schema = arrow::schema({int32_field, int64_field});
  json j = schema;
  json j_fields = {int32_field, int64_field};
  ASSERT_EQ(j.at("arrow_type"), "arrow_schema");
  ASSERT_EQ(j.at("fields"), j_fields);
}

TEST(SchemaTypeTest, MetadataSchemaTest) {
  auto int32_field = arrow::field("int32_field", arrow::int32());
  auto int64_field = arrow::field("int64_field", arrow::int64());
  auto schema = arrow::schema({int32_field, int64_field});
  auto keys = std::vector<std::string>{"one", "two", "three"};
  auto values = std::vector<std::string>{"1", "2", "3"};
  auto metadata = std::make_shared<const arrow::KeyValueMetadata>(keys, values);
  schema = schema->WithMetadata(metadata);
  json j = schema;
  json j_fields = {int32_field, int64_field};
  ASSERT_EQ(j.at("arrow_type"), "arrow_schema");
  ASSERT_EQ(j.at("fields"), j_fields);
  ASSERT_EQ(j.at("metadata"), json(metadata));
}
