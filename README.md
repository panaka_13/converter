# CONVERTER
### Requirements
* [apache arrow c++](https://github.com/apache/arrow/tree/master/cpp)
* [nlohmann json library](https://github.com/nlohmann/json)
* [google-test](https://github.com/google/googletest)

Install steps are listed futher in the appendix.

### Usage
Example code (example.cc)
```C++
#include <arrow/api.h>
#include <nlohmann/json.hpp>
#include "core.h"
#include <iostream>

using namespace std;

int main() {
  auto int32_type = arrow::int32();
  auto int32_field = field("int32_type", int32_type);
  auto int64_type = arrow::int64();
  auto list_type = std::make_shared<arrow::ListType>(int32_type);
  json j_type = int32_type;
  json j_64type = int64_type;
  json j_field = int32_field;
  json j_list = list_type;
  cout << j_type << endl;
  cout << j_64type << endl;
  cout << j_field << endl;
  cout << j_list << endl;
}
```
### Build, Test, and Run
Project structure
```
project
│   CMakeLists.txt
│   example.cc 
└───converter 
```
CMake
```CMake
cmake_minimum_required(VERSION 3.16.3)

project(example)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_package(Arrow REQUIRED)
find_package(nlohmann_json REQUIRED)

set(CMAKE_CXX_STANDARD 14)

include_directories(${CMAKE_SOURCE_DIR}/converter/include)
add_subdirectory(${CMAKE_SOURCE_DIR}/converter)
add_executable(example main.cc)

target_link_libraries(example PRIVATE core)
target_link_libraries(example PRIVATE nlohmann_json::nlohmann_json)
if (ARROW_LINK_SHARED)
  target_link_libraries(example PRIVATE arrow_shared)
else()
  set(THREADS_PREFER_PTHREAD_FLAG ON)
  find_package(Threads REQUIRED)
  target_link_libraries(example PRIVATE arrow_static Threads::Threads)
endif()
```
Build
```bash
mkdir build
cd build
cmake ..
make example
./example
```
Output
```
{"arrow_type":"arrow_datatype","type":7}
{"arrow_type":"arrow_datatype","type":9}
{"arrow_type":"arrow_field","name":"int32_type","nullable":true,"type":{"arrow_type":"arrow_datatype","type":7}}
{"arrow_type":"arrow_datatype","field":{"arrow_type":"arrow_field","name":"item","nullable":true,"type":{"arrow_type":"arrow_datatype","type":7}},"type":25}
```

### Appendix 1: installing required libraries
Install [apache arrow c++](https://github.com/apache/arrow/tree/master/cpp)
``` shell
cd arrow/cpp
mkdir build
cd build
cmake ..
make
make install
```

Install [nlohmann json library](https://github.com/nlohmann/json)
```bash
git clone https://github.com/nlohmann/json
mkdir build
cd build
cmake -DJSON_BuildTests=OFF ..
make
make install
```

For testing, install [google-test](https://github.com/google/googletest)
```bash
git clone https://github.com/google/googletest
mkdir build
cd build
cmake ..
make
make install
```
