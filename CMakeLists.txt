cmake_minimum_required(VERSION 3.16.3)

project(converter)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_package(Arrow REQUIRED)
find_package(nlohmann_json REQUIRED)

set(CMAKE_CXX_STANDARD 14)

include_directories(${CMAKE_SOURCE_DIR}/include)

add_subdirectory(src)
add_subdirectory(test)

add_executable(arrow_example example.cc)

target_link_libraries(arrow_example PRIVATE core)
target_link_libraries(arrow_example PRIVATE nlohmann_json::nlohmann_json)
target_link_libraries(test PRIVATE gtest gmock gtest_main)
target_link_libraries(test PRIVATE core)

if (ARROW_LINK_SHARED)
  target_link_libraries(arrow_example PRIVATE arrow_shared)
  target_link_libraries(test PRIVATE arrow_shared)
else()
  set(THREADS_PREFER_PTHREAD_FLAG ON)
  find_package(Threads REQUIRED)
  target_link_libraries(arrow_example PRIVATE arrow_static Threads::Threads)
  target_link_libraries(test PRIVATE arrow_static Threads::Threads)
endif()
