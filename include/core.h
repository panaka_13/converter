#pragma once

#include <arrow/api.h>
#include <memory>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace nlohmann {
template <>
struct adl_serializer<std::shared_ptr<const arrow::KeyValueMetadata>> {
  static void to_json(json &,
                      const std::shared_ptr<const arrow::KeyValueMetadata> &);
};

template <> struct adl_serializer<std::shared_ptr<arrow::Field>> {
  static void to_json(json &, const std::shared_ptr<arrow::Field> &);
};

template <> struct adl_serializer<std::shared_ptr<arrow::ListType>> {
  static void to_json(json &, const std::shared_ptr<arrow::ListType> &);
};

template <> struct adl_serializer<std::shared_ptr<arrow::DataType>> {
  static void to_json(json &, const std::shared_ptr<arrow::DataType> &);
};

template <> struct adl_serializer<std::shared_ptr<arrow::Schema>> {
  static void to_json(json &, const std::shared_ptr<arrow::Schema> &);
};
} // namespace nlohmann
