#include <arrow/api.h>
#include <nlohmann/json.hpp>

#include "core.h"

#include <iostream>

using namespace std;

int main() {
  auto int32_type = arrow::int32();
  auto int32_field = field("int32_type", int32_type);
  auto int64_type = arrow::int64();
  auto list_type = std::make_shared<arrow::ListType>(int32_type);
  json j_type = int32_type;
  json j_64type = int64_type;
  json j_field = int32_field;
  json j_list = list_type;
  cout << j_type << endl;
  cout << j_64type << endl;
  cout << j_field << endl;
  cout << j_list << endl;
}
